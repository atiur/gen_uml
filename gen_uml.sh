#!/bin/sh
gen_uml_dir=$(which gen_uml.sh 2>/dev/null)
if [ "$gen_uml_dir" == "" ] ; then
    echo "gen_uml.sh is not in your \$PATH. Please do not run it from current directory to avoid its own code to be included during parsing."
    exit
fi
gen_uml_dir=$(dirname $gen_uml_dir)
python "$gen_uml_dir/gen_uml.py" | sort | uniq | grep -v 'External class'


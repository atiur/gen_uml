import os
import re

def print_class_line(class_name, sup_name, att_list, func_list):
    global classes
    att_list = att_list.difference(func_list)
    print "[%s|%s|%s]" % (class_name, ";".join(att_list), ";".join([func+"()" for func in func_list]))
    if sup_name:
        print "[%s]^-[%s]" % (sup_name, class_name)

    if len(att_list) > 0:
        for att in att_list:
            if att.capitalize() in classes.keys():
                print "[%s]-%s>[%s]" % (class_name, att, att.capitalize())
            else:
                print "[%s]-%s>[%s]" % (class_name, att, att.capitalize())
                print "[%s]-[note:External class.{bg:wheat}]" % (att.capitalize())

classes = {}

def gen_uml():
    global classes
    regex_py = re.compile('.+\.py$')
    regex_test_py = re.compile('Test.+\.py$')
    regex_self = re.compile('self\.')
    file_names = [file_name for file_name in os.listdir(".") if regex_py.match(file_name) and not regex_test_py.match(file_name) ]
    #file_names = ["TestProcess.py"]
    for file_name in file_names:
        class_name, sup_name, att_list, func_list = None, None, set(), set()
        with open(file_name, 'rt') as file:
            for line in file:
                tokens = [token.strip() for token in line.split()]
                if len(tokens) > 1 and tokens[0] == 'class':
                    if class_name:
                        classes[class_name] = (sup_name, att_list, func_list)
                        class_name, sup_name, att_list, func_list = None, None, set(), set()

                    tokens = tokens[1].split(':')[0]
                    class_name = tokens.split('(')
                    if len(class_name) > 1:
                        sup_name = class_name[1].split(')')[0]
                    class_name = class_name[0]
                    
                elif len(tokens) > 1 and tokens[0] == 'def':
                    func_name = tokens[1].split('(')
                    if len(func_name) > 0:
                        func_list.add(func_name[0])
                elif len(tokens) > 0 and regex_self.search(tokens[0]):
                    atts = re.findall("self\.[a-z,_]+[a-z,A-Z,0-9,_]+", tokens[0])
                    atts = [att[5:].strip(",") for att in atts]
                    for att in atts:
                        pattern = "self\." + att + "\("
                        if not re.search(pattern, line):
                            att_list.add(att)
        if class_name:
            classes[class_name] = (sup_name, att_list, func_list)
    
    for class_name in classes.keys():
        if class_name and class_name != "None":
            print_class_line(class_name, (classes[class_name])[0], (classes[class_name])[1], (classes[class_name])[2])
                 
                    
            

if __name__ == "__main__":
    gen_uml()
